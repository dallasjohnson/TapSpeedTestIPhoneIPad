# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Testing iOS tap response times? ###

From my observations the tap timing resolution using the iPhone 5S (8.1.3)  synchronises closely to the screen refresh rate (60Hz) which makes sense but exactly the same code run on the iPad is double that at (120Hz) 

### How did I get these results? ###

1. Install and run on the devices as keeping tethered to XCode (tested on 8.15+)
  - The screen should be a plain grey with no UI elements.
2. Randomly tap on the screen and timings will be output to the console in Xcode. These times are measured from the `CADisplaylink` fire timestamp and the touch timestamp.
By toggling the `useCAMediaTimeReference` in code an re-running this will measure between the displaylink and CAMediaTime instead (although the output looks very similar anyway). The timing reference is also reset after 3 frames so that when graphed you can see the stepping effect better. It's also more indicative of the intended use case of measuring human reaction times.
3. Then the output in the console was copied into Excel ( This was just a quick and dirty way to chart the results)
4. In Excel, I sorted the input into `lifts` and `taps`
5. Then sorted within each group by reaction time.
6. I graphed each column using a simple line graph.

If there were enough sample and my physical tapping timing was random then I would expect to see somthing fairly close to a straight line on the graph for a high resolution system. as can be observed from the included Excel file there are distinct steps in the graph. On the iPhone Sheet the distance between the steps is roughly 16ms (1 frame @ 60FPS) and using the iPad the same step is roughly 8ms (0.5 frames @ 60Hz).